//
//  ARNAppDelegate.h
//  ARNAnalytics
//
//  Created by CocoaPods on 11/04/2014.
//  Copyright (c) 2014 xxxAIRINxxx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
