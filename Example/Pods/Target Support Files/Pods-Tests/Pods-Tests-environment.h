
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ARNAnalytics
#define COCOAPODS_POD_AVAILABLE_ARNAnalytics
#define COCOAPODS_VERSION_MAJOR_ARNAnalytics 0
#define COCOAPODS_VERSION_MINOR_ARNAnalytics 1
#define COCOAPODS_VERSION_PATCH_ARNAnalytics 0

// Expecta
#define COCOAPODS_POD_AVAILABLE_Expecta
#define COCOAPODS_VERSION_MAJOR_Expecta 0
#define COCOAPODS_VERSION_MINOR_Expecta 3
#define COCOAPODS_VERSION_PATCH_Expecta 1

// Expecta+Snapshots
#define COCOAPODS_POD_AVAILABLE_Expecta_Snapshots
#define COCOAPODS_VERSION_MAJOR_Expecta_Snapshots 1
#define COCOAPODS_VERSION_MINOR_Expecta_Snapshots 2
#define COCOAPODS_VERSION_PATCH_Expecta_Snapshots 1

// FBSnapshotTestCase
#define COCOAPODS_POD_AVAILABLE_FBSnapshotTestCase
#define COCOAPODS_VERSION_MAJOR_FBSnapshotTestCase 1
#define COCOAPODS_VERSION_MINOR_FBSnapshotTestCase 3
#define COCOAPODS_VERSION_PATCH_FBSnapshotTestCase 0

// GoogleAnalytics-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_GoogleAnalytics_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_GoogleAnalytics_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_GoogleAnalytics_iOS_SDK 0
#define COCOAPODS_VERSION_PATCH_GoogleAnalytics_iOS_SDK 9

// Specta
#define COCOAPODS_POD_AVAILABLE_Specta
#define COCOAPODS_VERSION_MAJOR_Specta 0
#define COCOAPODS_VERSION_MINOR_Specta 2
#define COCOAPODS_VERSION_PATCH_Specta 1

