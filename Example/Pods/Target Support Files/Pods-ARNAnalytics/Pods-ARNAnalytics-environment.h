
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ARNAnalytics
#define COCOAPODS_POD_AVAILABLE_ARNAnalytics
#define COCOAPODS_VERSION_MAJOR_ARNAnalytics 0
#define COCOAPODS_VERSION_MINOR_ARNAnalytics 1
#define COCOAPODS_VERSION_PATCH_ARNAnalytics 0

// GoogleAnalytics-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_GoogleAnalytics_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_GoogleAnalytics_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_GoogleAnalytics_iOS_SDK 0
#define COCOAPODS_VERSION_PATCH_GoogleAnalytics_iOS_SDK 9

