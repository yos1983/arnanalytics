# ARNAnalytics

[![CI Status](http://img.shields.io/travis/xxxAIRINxxx/ARNAnalytics.svg?style=flat)](https://travis-ci.org/xxxAIRINxxx/ARNAnalytics)
[![Version](https://img.shields.io/cocoapods/v/ARNAnalytics.svg?style=flat)](http://cocoadocs.org/docsets/ARNAnalytics)
[![License](https://img.shields.io/cocoapods/l/ARNAnalytics.svg?style=flat)](http://cocoadocs.org/docsets/ARNAnalytics)
[![Platform](https://img.shields.io/cocoapods/p/ARNAnalytics.svg?style=flat)](http://cocoadocs.org/docsets/ARNAnalytics)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

* iOS 7.0+
* ARC

## Installation

ARNAnalytics is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "ARNAnalytics"

## Dependency

[GoogleAnalytics-iOS-SDK](https://developers.google.com/analytics/devguides/collection/ios/resources)

## License

ARNAnalytics is available under the MIT license. See the LICENSE file for more info.

