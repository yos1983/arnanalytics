//
//  ARNAnalytics.m
//  ARNAnalytics
//
//  Created by Airin on 2014/10/25.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import "ARNAnalytics.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "GAITracker.h"

@implementation ARNAnalytics

+ (void)setupGAWithTrackingId:(NSString *)trackingId
{
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval        = 20;
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    [[GAI sharedInstance] trackerWithTrackingId:trackingId];
}

+ (void)sendGAScreenName:(NSString *)screenName
{
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName value:screenName];
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createAppView] build]];
}

+ (void)sendGATrackWithCategoryName:(NSString *)categoryName
                         actionName:(NSString *)actionName
                          labelName:(NSString *)labelName
                              value:(NSNumber *)value
{
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:categoryName
                                                                           action:actionName
                                                                            label:labelName
                                                                            value:value];
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[builder build]];
}

+ (NSString *)dateToYYYYMMDDHHMM:(NSDate *)aDate
{
    NSDate *beforeDate = aDate;
    if (!beforeDate) {
        beforeDate = [NSDate date];
    }
    
    NSCalendar       *cal        = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:beforeDate];
    NSDate           *date       = [cal dateFromComponents:components];
    
    NSDateFormatter *formatter = [self defaultFormatter];
    [formatter setDateFormat:@"yyyyMMddHHmm"];
    
    return[formatter stringFromDate:date];
}

+ (NSDateFormatter *)defaultFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setCalendar:[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar]];
    
    return formatter;
}

@end
