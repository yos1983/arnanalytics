//
//  ARNAnalytics.h
//  ARNAnalytics
//
//  Created by Airin on 2014/10/25.
//  Copyright (c) 2014 Airin. All rights reserved.
//

@interface ARNAnalytics : NSObject

/**
 *  初期設定とトラッキングIDを設定.必ず最初に実行すること
 *
 *  @param trackingId トラッキングID
 */
+ (void)setupGAWithTrackingId:(NSString *)trackingId;

/**
 *  画面表示をトラッキングして送信
 *
 *  @param screenName 画面名
 */
+ (void)sendGAScreenName:(NSString *)screenName;

/**
 *  イベントをトラッキングして送信
 *
 *  @param categoryName カテゴリー名
 *  @param actionName   アクション名
 *  @param labelName    ラベル名
 *  @param value        任意値
 */
+ (void)sendGATrackWithCategoryName:(NSString *)categoryName
                         actionName:(NSString *)actionName
                          labelName:(NSString *)labelName
                              value:(NSNumber *)value;

@end
